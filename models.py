from sqlalchemy import (Integer,
                        Sequence,
                        create_engine,
                        String,
                        Column,
                        Numeric)

from sqlalchemy.dialects.postgresql import HSTORE
from sqlalchemy.ext.mutable import MutableDict
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy.orm import sessionmaker
from sqlalchemy.engine.url import URL

import settings

Base = declarative_base()

engine = create_engine(URL(**settings.DATABASE))

session = sessionmaker(bind=engine)()


class HStoreMixin(object):

    @declared_attr
    def data(self):
        return Column(MutableDict.as_mutable(HSTORE))

    _data_attributes = []

    def __init__(self, *args, **kwargs):
        super(HStoreMixin, self).__init__(*args, **kwargs)

        if not self.data:
            self.data = {}

    def __getattr__(self, name):
        if name in self._data_attributes:
            return self.data.get(name, '').decode('utf8')

        raise AttributeError(name)

    def __setattr__(self, name, value):
        if name in self._data_attributes:
            self.data[name] = unicode(value).encode('utf8')
            return

        super(HStoreMixin, self).__setattr__(name, value)

    def __delattr__(self, name):
        if name in self._data_attributes:
            del self.data[name]
            return

        super(HStoreMixin, self).__delattr__(name)


class Product(HStoreMixin, Base):
    TYPE_TABLET = 10
    TYPE_TSHIRT = 20

    __tablename__ = 'products'
    id = Column(Integer, Sequence('product_id_seq'), primary_key=True)
    name = Column(String(256), nullable=False)
    quantity = Column(Integer, nullable=False)
    price = Column(Numeric, nullable=False)
    type = Column(Integer, nullable=False)

    __mapper_args__ = {
        'polymorphic_on': type,
    }


class Tablet(Product):
    _data_attributes = ['resolution', 'os', 'memory', 'processor_speed']

    __mapper_args__ = {
        'polymorphic_identity': Product.TYPE_TABLET
    }


class TShirt(Product):
    _data_attributes = ['size', 'colour', 'collar']

    __mapper_args__ = {
        'polymorphic_identity': Product.TYPE_TSHIRT
    }
