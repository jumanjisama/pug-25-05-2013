--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO products (id, name, quantity, price, type, data) VALUES (1, 'zoro', 10, 12, 20, '"size"=>"XL", "colour"=>"yellow"');
INSERT INTO products (id, name, quantity, price, type, data) VALUES (2, 'zoro', 10, 12, 20, '"size"=>"L", "colour"=>"yellow"');
INSERT INTO products (id, name, quantity, price, type, data) VALUES (3, 'luffy', 10, 12, 20, '"size"=>"S", "colour"=>"green"');
INSERT INTO products (id, name, quantity, price, type, data) VALUES (4, 'luffy', 10, 12, 20, '"size"=>"M", "colour"=>"blue"');
INSERT INTO products (id, name, quantity, price, type, data) VALUES (5, 'nami', 10, 12, 20, '"size"=>"S", "colour"=>"white"');
INSERT INTO products (id, name, quantity, price, type, data) VALUES (6, 'iPad', 10, 499.99, 10, '"os"=>"iOS5", "memory"=>"64GB", "resolution"=>"2048x1536", "processor_speed"=>"1GHz"');
INSERT INTO products (id, name, quantity, price, type, data) VALUES (7, 'Microsoft Surface', 10, 499, 10, '"os"=>"Microsoft RT", "memory"=>"64GB", "resolution"=>"1366x768", "processor_speed"=>"1.3GHz"');
INSERT INTO products (id, name, quantity, price, type, data) VALUES (8, 'Samsung Galaxy Tab 10.1', 10, 374.99, 10, '"os"=>"Android 4.0", "memory"=>"32GB", "resolution"=>"1280x800", "processor_speed"=>"1GHz"');
INSERT INTO products (id, name, quantity, price, type, data) VALUES (9, 'Nexus 7', 10, 245, 10, '"os"=>"Android 4.1", "memory"=>"16GB", "resolution"=>"1280x800", "processor_speed"=>"1.3GHz"');
INSERT INTO products (id, name, quantity, price, type, data) VALUES (10, 'Sony Xperia Tablet S', 10, 498, 10, '"os"=>"Android 4.0", "memory"=>"16GB", "resolution"=>"1280x800", "processor_speed"=>"1.4GHz"');


--
-- PostgreSQL database dump complete
--

